<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        for ($i = 1; $i <= 50000; $i++) {

            DB::table('customers')->insert([
                'nik' => $faker->creditCardNumber,
                'name' => $faker->name,
                'address' => $faker->address,
                'phone' => $faker->phoneNumber,
                'point' => $faker->postcode,
                'created_at' => $faker->dateTime('now', 'Asia/Bangkok'),
                'updated_at' => $faker->dateTime('now', 'Asia/Bangkok')
            ]);
        }
    }
}
