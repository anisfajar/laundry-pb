<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// menambahkan route untuk customer
Route::get('/customer', 'CustomerController@all')->middleware('jwt.verify');
Route::post('/customer', 'CustomerController@store');
Route::get('/customer-all', 'CustomerController@getall');
Route::get('/customer/{id}', 'CustomerController@show');
Route::get('/customer-nik/{nik}', 'CustomerController@getbynik');
Route::put('/customer/{id}', 'CustomerController@update');
Route::delete('/customer/{id}', 'CustomerController@delete');

// menambahkan route untuk detail transaction
Route::get('/det-transc', 'DetailTransactionController@all');
Route::post('/det-transc', 'DetailTransactionController@store');

// menambahkan route untuk expense
Route::get('/expense', 'ExpenseController@all')->middleware('jwt.verify');
Route::post('/expense', 'ExpenseController@store');
Route::get('/expense/{id}', 'ExpenseController@show');
Route::put('/expense/{id}', 'ExpenseController@update');
Route::delete('/expense/{id}', 'ExpenseController@delete');

// menambahkan route untuk laundry price
Route::get('/laund-price', 'LaundryPriceController@all')->middleware('jwt.verify');
Route::post('/laund-price', 'LaundryPriceController@store');
Route::get('/laund-priceall', 'LaundryPriceController@getall');
Route::get('/laund-price/{id}', 'LaundryPriceController@show');
Route::put('/laund-price/{id}', 'LaundryPriceController@update');
Route::delete('/laund-price/{id}', 'LaundryPriceController@delete');

// menambahkan route untuk laundry type
/* Route::get('/laund-type', 'LaundryTypeController@all');
Route::get('/laund-typeall', 'LaundryTypeController@alldata');
Route::post('/laund-type', 'LaundryTypeController@store');
Route::get('/laund-type/{id}', 'LaundryTypeController@show');
Route::put('/laund-type/{id}', 'LaundryTypeController@update'); */

// menambahkan route untuk outlet
Route::get('/outlets', 'OutletController@allAuth')->middleware('jwt.verify');
// Route::get('/outlets', 'OutletController@allAuth');
Route::post('/outlets', 'OutletController@simpan');
Route::get('/outlets/{id}', 'OutletController@show');
Route::put('/outlets/{id}', 'OutletController@update');
Route::delete('/outlets/{id}', 'OutletController@delete');

// menambahkan route untuk transaction
Route::get('/transc', 'TransactionController@all')->middleware('jwt.verify');
Route::get('/transclastque', 'TransactionController@lastque');
Route::post('/transc', 'TransactionController@store');
Route::put('/transc/{id}', 'TransactionController@update');

// menambahkan route untuk user
Route::get('/user', 'UserController@all');
Route::post('/user', 'UserController@store');
Route::put('/user/{id}', 'UserController@update');

//menambahkan route untuk dashboard
Route::get('/jmldata', 'DashboardController@jmldata')->middleware('jwt.verify');

Route::prefix('auth')->group(function () {
    // Below mention routes are public, user can access those without any restriction.
    // Create New User
    Route::post('register', 'AuthController@register');
    // Login User
    Route::post('login', 'AuthController@login');

    // Refresh the JWT Token
    Route::get('refresh', 'AuthController@refresh');

    // Below mention routes are available only for the authenticated users.
    Route::middleware('auth:api')->group(function () {
        // Get user info
        Route::get('user', 'AuthController@user');
        // Logout user from application
        Route::post('logout', 'AuthController@logout');
    });
});


Route::get('outletall', 'OutletController@OutletAuth')->middleware('jwt.verify');
