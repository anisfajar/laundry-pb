<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\DetailTransaction;

class DetailTransactionController extends Controller
{
    // mengambil semua data
    public function all()
    {
        return DetailTransaction::all();
    }

    // menambah data
    public function store(Request $request)
    {
        $dettransc = new DetailTransaction;
        $dettransc->transaction_id = $request->idlastque;
        $dettransc->laundry_price_id = $request->layanan;
        $dettransc->qty = $request->qty;
        $dettransc->subtotal = str_replace(".", "", $request->subtotal);

        $dettransc->save();
    }
}
