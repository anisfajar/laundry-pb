<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    // mengambil semua data
    public function all(Request $request)
    {
        if ($request->input('client')) {
            return Customer::select('id', 'nik', 'name', 'address', 'phone', 'point')->get();
        }
        $columns = ['id', 'nik', 'name', 'address', 'phone', 'point'];

        $length = $request->input('length');
        $column = $request->input('column'); //Index
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = Customer::select('id', 'nik', 'name', 'address', 'phone', 'point')->orderBy($columns[$column], $dir);
        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('nik', 'like', '%' . $searchValue . '%')
                    ->orWhere('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('address', 'like', '%' . $searchValue . '%')
                    ->orWhere('phone', 'like', '%' . $searchValue . '%')
                    ->orWhere('point', 'like', '%' . $searchValue . '%');
            });
        }

        $projectdata = $query->paginate($length);
        return [
            'data' => $projectdata,
            'draw' => $request->input('draw')
        ];
    }

    // menambah data
    public function store(Request $request)
    {
        $cust = new Customer;
        $cust->nik = $request->nik;
        $cust->name = $request->nama;
        $cust->address = $request->alamat;
        $cust->phone = $request->telp;
        $cust->point = $request->point;

        $cust->save();
    }


    //getdata by id
    public function show($id)
    {
        return Customer::find($id);
    }

    public function getbynik($nik)
    {
        return Customer::select('id', 'nik', 'name')->where('nik', $nik)->get();
    }

    public function getall()
    {
        return Customer::all();
    }

    // mengubah data
    public function update($id, Request $request)
    {
        $customer = Customer::find($id);
        $customer->update($request->all());
        return $customer;
    }

    // menghapus data
    public function delete($id)
    {
        $outlet = Customer::find($id);
        $outlet->delete();
        return 204;
    }
}
