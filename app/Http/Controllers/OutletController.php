<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Outlet;

class OutletController extends Controller
{
    // mengambil semua data
    public function allAuth(Request $request)
    {
        if ($request->input('client')) {
            return Outlet::select('id', 'code', 'name', 'address', 'phone')->get();
        }
        $columns = ['id', 'code', 'name', 'address', 'phone'];

        $length = $request->input('length');
        $column = $request->input('column'); //Index
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = Outlet::select('id', 'code', 'name', 'address', 'phone')->orderBy($columns[$column], $dir);
        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('code', 'like', '%' . $searchValue . '%')
                    ->orWhere('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('address', 'like', '%' . $searchValue . '%')
                    ->orWhere('phone', 'like', '%' . $searchValue . '%');
            });
        }

        $projectdata = $query->paginate($length);
        return [
            'data' => $projectdata,
            'draw' => $request->input('draw')
        ];

        /* $data = Outlet::all();
        return response()->json($data, 200); */
    }

    public function OutletAuth()
    {
        $data = "Welcome " . Auth::user()->name;
        return response()->json($data, 200);
    }

    // menambah data
    public function simpan(Request $request)
    {
        $outlet = new Outlet;
        $outlet->code = $request->kode;
        $outlet->name = $request->nama;
        $outlet->address = $request->alamat;
        $outlet->phone = $request->telp;

        $outlet->save();
    }

    // mengubah data
    public function update($id, Request $request)
    {
        $outlet = Outlet::find($id);
        $outlet->update($request->all());
        return $outlet;
    }

    //getdata by id
    public function show($id)
    {
        return Outlet::find($id);
    }

    // menghapus data
    public function delete($id)
    {
        $outlet = Outlet::find($id);
        $outlet->delete();
        return 204;
    }
}
