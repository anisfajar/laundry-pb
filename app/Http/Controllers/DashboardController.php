<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Outlet;
use App\Transaction;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function jmldata()
    {
        $cust = Customer::all();
        $outl = Outlet::all();
        $allbiaya = DB::table("expenses")->sum('price');
        $transc = Transaction::all();

        $alldata = [
            'customer_count' => $cust->count(),
            'outlet_count' => $outl->count(),
            'totbiaya_keluar' => $allbiaya,
            'transc_count' => $transc->count(),
        ];

        return $alldata;
    }
}
