<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

class UserController extends Controller
{
    // mengambil semua data
    public function all()
    {
        return User::all();
    }

    // menambah data
    public function store(Request $request)
    {
        return User::create($request->all());
    }

    // mengubah data
    public function update($id, Request $request)
    {
        $user = User::find($id);
        $user->update($request->all());
        return $user;
    }
}
