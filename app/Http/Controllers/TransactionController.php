<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\DetailTransaction;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    // mengambil semua data
    public function all(Request $request)
    {
        if ($request->input('client')) {
            // return Transaction::select('id', 'customer_id', 'amount', 'start_date', 'end_date')->get();
            return DB::table('transactions')
                ->join('customers', 'customers.id', '=', 'transactions.customer_id')
                ->join('detail_transactions', 'detail_transactions.transaction_id', '=', 'transactions.id')
                ->select('customers.*', 'transactions.start_date', 'transactions.end_date', 'detail_transactions.qty', 'detail_transactions.subtotal')
                ->get();
        }
        $columns        = ['id', 'nik', 'name', 'point', 'phone', 'start_date', 'end_date', 'qty', 'subtotal'];

        $length         = $request->input('length');
        $column         = $request->input('column'); //index
        $dir            = $request->input('dir');
        $searchValue    = $request->input('search');

        // $query = Transaction::select('id', 'customer_id', 'amount', 'start_date', 'end_date')->orderBy($columns[$column], $dir);
        $query = DB::table('transactions')
            ->join('customers', 'customers.id', '=', 'transactions.customer_id')
            ->join('detail_transactions', 'detail_transactions.transaction_id', '=', 'transactions.id')
            ->select('customers.*', 'transactions.start_date', 'transactions.end_date', 'detail_transactions.qty', 'detail_transactions.subtotal')
            ->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('nik', 'like', '%' . $searchValue . '%')
                    ->orWhere('point', 'like', '%' . $searchValue . '%')
                    ->orWhere('phone', 'like', '%' . $searchValue . '%')
                    ->orWhere('start_date', 'like', '%' . $searchValue . '%')
                    ->orWhere('qty', 'like', '%' . $searchValue . '%')
                    ->orWhere('subtotal', 'like', '%' . $searchValue . '%')
                    ->orWhere('end_date', 'like', '%' . $searchValue . '%');
            });
        }

        $projectdata = $query->paginate($length);
        return [
            'data' => $projectdata,
            'draw' => $request->input('draw')
        ];
    }

    // menambah data
    public function store(Request $request)
    {
        $transc = new Transaction;
        $transc->customer_id = $request->idcust;
        $transc->start_date = $request->tglmulai;
        $transc->end_date = $request->tglselesai;

        $transc->save();
    }

    // mengubah data
    public function update($id, Request $request)
    {
        $transaction = Transaction::find($id);
        $transaction->update($request->all());
        return $transaction;
    }

    public function lastque()
    {
        // $sql = DB::query("SELECT AUTO_INCREMENT AS urut FROM information_schema.TABLES WHERE TABLE_SCHEMA = 'dw-laundry' AND TABLE_NAME = 'transactions' ");

        $query = DB::table('information_schema.TABLES');
        $query->select('AUTO_INCREMENT as urut');
        $query->where('TABLE_SCHEMA', 'dw-laundry');
        $query->where('TABLE_NAME', 'transactions');
        return $query->get();
    }
}
