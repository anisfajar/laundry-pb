<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;

class ExpenseController extends Controller
{
    // mengambil semua data
    public function all(Request $request)
    {
        if ($request->input('client')) {
            return Expense::select('id', 'description', 'price', 'note', 'periode')->get();
        }
        $columns = ['id', 'description', 'price', 'note', 'periode'];

        $length = $request->input('length');
        $column = $request->input('column'); //Index
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = Expense::select('id', 'description', 'price', 'note', 'periode')->orderBy($columns[$column], $dir);
        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('description', 'like', '%' . $searchValue . '%')
                    ->orWhere('price', 'like', '%' . $searchValue . '%')
                    ->orWhere('note', 'like', '%' . $searchValue . '%')
                    ->orWhere('periode', 'like', '%' . $searchValue . '%');
            });
        }

        $projectdata = $query->paginate($length);
        return [
            'data' => $projectdata,
            'draw' => $request->input('draw')
        ];
    }

    // menambah data
    public function store(Request $request)
    {
        $expense = new Expense;
        $expense->description = $request->deskripsi;
        $expense->price = $request->harga;
        $expense->note = $request->catatan;
        $expense->periode = $request->periode;

        $expense->save();
    }

    // mengubah data
    public function update($id, Request $request)
    {
        $expense = Expense::find($id);
        $expense->update($request->all());
        return $expense;
    }

    //getdata by id
    public function show($id)
    {
        return Expense::find($id);
    }

    // menghapus data
    public function delete($id)
    {
        $outlet = Expense::find($id);
        $outlet->delete();
        return 204;
    }
}
