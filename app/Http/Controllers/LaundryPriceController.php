<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LaundryPrice;

class LaundryPriceController extends Controller
{
    // mengambil semua data
    public function all(Request $request)
    {
        if ($request->input('client')) {
            return LaundryPrice::select('id', 'name', 'unit_type', 'price')->get();
        }
        $columns = ['id', 'name', 'unit_type', 'price'];

        $length = $request->input('length');
        $column = $request->input('column'); //Index
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = LaundryPrice::select('id', 'name', 'unit_type', 'price')->orderBy($columns[$column], $dir);
        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('name', 'like', '%' . $searchValue . '%')
                    ->orWhere('unit_type', 'like', '%' . $searchValue . '%')
                    ->orWhere('price', 'like', '%' . $searchValue . '%');
            });
        }

        $projectdata = $query->paginate($length);
        return [
            'data' => $projectdata,
            'draw' => $request->input('draw')
        ];
    }

    // menambah data
    public function store(Request $request)
    {
        $harga = new LaundryPrice;
        $harga->name = $request->nama;
        $harga->unit_type = 'Kg';
        $harga->price = $request->harga;

        $harga->save();
    }

    // mengubah data
    public function update($id, Request $request)
    {
        $laundryprice = LaundryPrice::find($id);
        $laundryprice->update($request->all());
        return $laundryprice;
    }

    //getdata by id
    public function show($id)
    {
        return LaundryPrice::find($id);
    }

    //ambil semua data
    public function getall()
    {
        return LaundryPrice::all();
    }

    // menghapus data
    public function delete($id)
    {
        $outlet = LaundryPrice::find($id);
        $outlet->delete();
        return 204;
    }
}
