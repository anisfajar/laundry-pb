<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LaundryPrice extends Model
{
    protected $table = 'laundry_prices';
    protected $fillable = ['id', 'name', 'unit_type', 'price', 'created_at', 'updated_at'];
}
