<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    protected $fillable = ['id', 'nik', 'name', 'address', 'phone', 'point', 'created_at', 'updated_at'];
}
