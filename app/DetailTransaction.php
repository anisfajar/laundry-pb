<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailTransaction extends Model
{
    protected $table = 'detail_transactions';
    protected $fillable = ['id', 'transaction_id', 'laundry_price_id',  'qty',  'subtotal', 'created_at', 'updated_at'];
}
