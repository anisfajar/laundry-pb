<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $table = 'expenses';
    protected $fillable = ['id', 'description', 'price', 'note', 'periode', 'created_at', 'updated_at'];
}
