<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';
    protected $fillable = ['id', 'customer_id', 'start_date', 'end_date', 'status', 'created_at', 'updated_at'];
}
