<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outlet extends Model
{
    protected $table = 'outlets';
    protected $fillable = ['id', 'code', 'name', 'status', 'address', 'phone', 'created_at', 'updated_at'];
}
