require('./bootstrap');
window.Vue = require('vue');

import VueAxios from 'vue-axios';
import Axios from 'axios';
import Vuex from 'vuex'

Vue.use(VueAxios, Axios, Vuex);

export default new Vuex.Store({
    state: {
        status: '',
        token: localStorage.getItem('token') || ''
    },
    getters: {
        isLoggedIn: state => !!state.token,
        authStatus: state => state.status,
    },
    mutations: {

    },
    actions: {

    },
})