require('./bootstrap');
window.Vue = require('vue');

// import dependecies tambahan
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import Axios from 'axios';
import VueSweetalert2 from 'vue-sweetalert2';
import Swal from 'sweetalert';
import Swal2 from 'sweetalert2'
import VueAuth from 'vue-auth';

Vue.use(VueRouter, VueAxios, Axios, Swal, VueSweetalert2, Swal2, VueAuth);

Vue.component('pagination', require('laravel-vue-pagination'));

// import file yang dibuat tadi

import router from './app.js';
import store from './store.js';
import App from './components/App.vue';
import Dashboard from './components/Dashboard.vue';

//pelanggan
import Pelanggan from './components/pages/customer/v_read.vue';
import AddPelanggan from './components/pages/customer/v_add.vue';
import EditPelanggan from './components/pages/customer/v_edit.vue';
//--pelanggan


//outlet
import Outlet from './components/pages/outlets/v_read.vue';
import AddOutlet from './components/pages/outlets/v_add.vue';
import EditOutlet from './components/pages/outlets/v_edit.vue';
//--outlet

//expense
import Expense from './components/pages/expense/v_read.vue';
import AddExpense from './components/pages/expense/v_add.vue';
import EditExpense from './components/pages/expense/v_edit.vue';
//--expense

//harga layanan (laundry prices)
import Laundryp from './components/pages/laundry_prices/v_read.vue';
import AddLaundryp from './components/pages/laundry_prices/v_add.vue';
import EditLaundryp from './components/pages/laundry_prices/v_edit.vue';
//harga layanan (laundry prices)


//transaction 
import Transac from './components/pages/transaction/v_read.vue';
import AddTransac from './components/pages/transaction/v_add.vue';
import DetailTransac from './components/pages/transaction/v_detail.vue';

//transaction

export default new VueRouter({
    mode: 'history',
    routes: [{
            name: 'dashboard',
            path: '/beranda',
            component: Dashboard,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'customer',
            path: '/customer',
            component: Pelanggan,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'update',
            path: '/ecustomer/:id',
            component: EditPelanggan,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'addcustomer',
            path: '/acustomer',
            component: AddPelanggan,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'outlet',
            path: '/outlet',
            component: Outlet,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'addoutlet',
            path: '/aoutlet',
            component: AddOutlet,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'editoutlet',
            path: '/eoutlet/:id',
            component: EditOutlet,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'expense',
            path: '/expense',
            component: Expense,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'addexpense',
            path: '/aexpense',
            component: AddExpense,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'editexpense',
            path: '/eexpense/:id',
            component: EditExpense,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'harga',
            path: '/harga',
            component: Laundryp,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'addharga',
            path: '/aharga',
            component: AddLaundryp,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'editharga',
            path: '/eharga/:id',
            component: EditLaundryp,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'transac',
            path: '/transac',
            component: Transac,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'addtransac',
            path: '/atransac',
            component: AddTransac,
            meta: {
                requiresAuth: true
            }
        },
        {
            name: 'detailtransac',
            path: '/dtransac/:id',
            component: DetailTransac,
            meta: {
                requiresAuth: true
            }
        },
    ]
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) { // jika user sudah login maka route akan diteruskan
            next()
            return
        }
        window.location.href = "/";
    } else {
        next()
    }
})
new Vue(Vue.util.extend({ router }, App)).$mount("#app");