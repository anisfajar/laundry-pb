require('./bootstrap');
window.Vue = require('vue');

// import dependecies tambahan
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import Axios from 'axios';
import VueSweetalert2 from 'vue-sweetalert2';
import Swal from 'sweetalert';
import Swal2 from 'sweetalert2'

Vue.use(VueRouter, VueAxios, Axios, Swal, VueSweetalert2, Swal2);

import router from './applogin.js';
import Applogin from './components/Applogin.vue';

export default new VueRouter({
    mode: 'history',
    routes: [{
        name: 'loginapp',
        path: '/',
        component: Applogin
    }]
});
new Vue(Vue.util.extend({ router }, Applogin)).$mount("#app-login");