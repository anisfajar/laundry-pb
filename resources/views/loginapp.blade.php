<!DOCTYPE html>
<html>

<head>
    <title>Silahkan Login</title>
    <link rel="stylesheet" type="text/css" href="{{asset('applogin/css/style.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet">
    <script src="{{asset('applogin/js/kit.js')}}"></script>
    <script src="{{mix('js/applogin.js')}}" type="text/javascript" defer></script>
    <!-- <script src="{{mix('js/app.js')}}" type="text/javascript" defer></script> -->

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <div id="app-login">

    </div>
    <script type="text/javascript" src="{{asset('applogin/js/main.js')}}"></script>
</body>

</html>