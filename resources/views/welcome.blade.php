<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <title>LaundryKu</title>
    <meta name="viewport" content="width=device-width">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300' rel='stylesheet' type='text/css'>
    <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet" />
    <script src="{{ mix('js/app.js') }}" type="text/javascript" defer></script>

    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('res/vendors/mdi/css/materialdesignicons.min.css')}}">

    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css"> -->
    <link rel="stylesheet" href="{{asset('res/vendors/simple-line-icons/css/simple-line-icons.css')}}">
    <link rel="stylesheet" href="{{asset('res/vendors/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('res/vendors/css/vendor.bundle.base.css')}}">
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css" /> -->
    <link rel="stylesheet" href="{{asset('res/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}" />
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="{{asset('res/vendors/font-awesome/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('res/vendors/jquery-bar-rating/fontawesome-stars.css')}}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('res/css/style.css')}}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{asset('res/images/minilogo.png')}}" />

</head>

<body>
    <div id="app">
    </div>

    <!-- plugins:js -->
    <script src="{{asset('res/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script> -->

    <script src="{{asset('res/vendors/twbs-pagination/jquery.twbsPagination.min.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <script src="{{asset('res/vendors/datatables.net/jquery.dataTables.js')}}"></script>
    <script src="{{asset('res/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('res/vendors/jquery-bar-rating/jquery.barrating.min.js')}}"></script>
    <script src="{{asset('res/vendors/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('res/vendors/raphael/raphael.min.js')}}"></script>
    <script src="{{asset('res/vendors/morris.js/morris.min.js')}}"></script>
    <script src="{{asset('res/vendors/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{asset('res/js/off-canvas.js')}}"></script>
    <script src="{{asset('res/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('res/js/misc.js')}}"></script>
    <script src="{{asset('res/js/settings.js')}}"></script>
    <script src="{{asset('res/js/todolist.js')}}"></script>

    <script src="{{asset('res/js/tooltips.js')}}"></script>
    <script src="{{asset('res/js/popover.js')}}"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="{{asset('res/js/dashboard.js')}}"></script>
    <!-- End custom js for this page-->
</body>

</html>